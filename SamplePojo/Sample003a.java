class Sample003aAnimal {

    String animalType = null;

    public Sample003aAnimal(final String argType) {
        this.animalType = argType;
        System.out.println("I am " + this.animalType);
    }

    public void jump() {
        System.out.println(animalType + ":jump");
    }

}

class Sample003aCat extends Sample003aAnimal {

    public Sample003aCat() {
        super("cat");
    }

    public void punch() {
        System.out.println(super.animalType + ":punch");
    }
}

class Sample003aDog extends Sample003aAnimal {

    public Sample003aDog() {
        super("dog");
    }

    public void bow() {
        System.out.println(super.animalType + ":bow");
    }

}

public class Sample003a {
    public static void main(String[] args) {

        Sample003aDog dog = new Sample003aDog();
        Sample003aCat cat = new Sample003aCat();

        dog.jump();
        cat.jump();

        dog.bow();
        cat.punch();
    }
}