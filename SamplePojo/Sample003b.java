class Sample003bAnimal {

    String animalType = null;

    public Sample003bAnimal(final String argType) {
        this.animalType = argType;
        System.out.println("I am " + this.animalType);
    }

    public void jump() {
        System.out.println(animalType + ":jump");
    }

}

class Sample003bCat extends Sample003bAnimal {

    public Sample003bCat() {
        super("cat");
    }

    public void jump() {
        super.jump();
        System.out.println(super.animalType + ":Mya-----");
    }
}

class Sample003bDog extends Sample003bAnimal {

    public Sample003bDog() {
        super("dog");
    }
}

public class Sample003b {
    public static void main(String[] args) {

        Sample003bDog dog = new Sample003bDog();
        Sample003bCat cat = new Sample003bCat();

        dog.jump();
        cat.jump();

    }
}