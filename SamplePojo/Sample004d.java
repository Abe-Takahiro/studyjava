import java.util.HashMap;
import java.util.Iterator;

public class Sample004d {
    
    public static void main(String args[]) {
        
        HashMap<String,String> hmNames = new HashMap<String,String>();
        hmNames.put("1", "pluto");
        hmNames.put("2", "goofy");
        hmNames.put("3", "dnaldo duck");

        System.out.println("hsNames.size :" + hmNames.size());

        // While文
        Iterator<String> itKeys = hmNames.keySet().iterator();
        while(itKeys.hasNext()) {
            System.out.println(hmNames.get(itKeys.next()));
        }

        // for文
        for (String sKey : hmNames.keySet()) {
            System.out.println(hmNames.get(sKey));
        }

        // ラムダ式
        hmNames.forEach((key, val) -> System.out.println("Key:" + key + "/Val:" + val));
    }
}