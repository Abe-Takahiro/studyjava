import java.util.ArrayList;

public class Sample004b {

    public static void main(String[] args) {

        ArrayList<String> listNames = new ArrayList<String>();
        listNames.add("plute");
        listNames.add("goofy");
        listNames.add("dnaldo duck");

        System.out.println("listNames.size() :" + listNames.size());

        // For文(1)
        for (int i = 0; i < listNames.size(); i++) {
            System.out.println(listNames.get(i));
        }

        // For文(2)
        for (String name : listNames) {
            System.out.println(name);
        }

        // ラムダ式
        listNames.forEach( sName -> System.out.println(sName));
    }
}