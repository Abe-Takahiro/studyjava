/**
 * クラス（責務）
 */
public class Sample002 {
    /*
     * フィールド（属性） - greet
     */
    String greet = "Hello";
    /*
     * フィールド（属性） - name
     */
    String name = "world";

    /*
     * コンストラクタ
     */
    public Sample002() {
        System.out.println("Execute -Constructor");
    }

    /*
     * メソッド（振る舞い） - greeting
     */
    public void greeting() {
        System.out.println(greet + " " + name);
    }

    /*
     * メインメソッド（かなり特殊な静的な振る舞い） - main
     */
    public static void main(String[] args) {
        Sample002 sample002 = new Sample002();
        System.out.println(sample002);
        sample002.greeting();
    }
}