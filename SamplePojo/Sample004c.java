import java.util.HashSet;
import java.util.Iterator;

public class Sample004c {
    
    public static void main(String[] args) {
        HashSet<String> hsNames = new HashSet<String>();

        hsNames.add("pluto");
        hsNames.add("goofy");
        hsNames.add("dnaldo duck");

        System.out.println(hsNames.size());

        // while文
        Iterator<String> itNames = hsNames.iterator();
        while(itNames.hasNext()) {
            System.out.println(itNames.next());
        }

        // for文
        for (String sName : hsNames) {
            System.out.println(sName);
        }

        // ラムダ式
        hsNames.forEach(name -> System.out.println(name));

    }
}