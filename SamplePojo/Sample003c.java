class Sample003cAnimal {

    String animalType = null;

    String animalName = null;
    
    public Sample003cAnimal() {
        this("Anyone", null);
    }

    public Sample003cAnimal(String argAnimalType) {
        this(argAnimalType, null);
    }

    public Sample003cAnimal(String argAnimalType, String argAnimalName) {
        this.animalType = argAnimalType;
        this.animalName = argAnimalName;
    }

    public void greeting() {
        System.out.println("Hello, I'm " + this.animalType + ".");
        if (this.animalName != null) {
            System.out.println("Please call me  '" + this.animalName + "'.");
        } else {
            System.out.println("I have no name.");
        }
    }
}

public class Sample003c {
    public static void main(String[] args) {
        Sample003cAnimal pluto = new Sample003cAnimal("dog", "pluto");
        Sample003cAnimal noracat = new Sample003cAnimal("cat");
        Sample003cAnimal unknown = new Sample003cAnimal();

        pluto.greeting();
        noracat.greeting();
        unknown.greeting();
    }
}