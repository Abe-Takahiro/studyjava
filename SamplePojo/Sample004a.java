public class Sample004a {
    public static void main(String[] args) {
        String[] sNames = new String[3];
        sNames[0] = "pluto";
        sNames[1] = "goofy";
        sNames[2] = "donald duck";

        // for文
        System.out.println("sNames length:" + sNames.length);
        for (int i = 0; i< sNames.length; i++) {
            System.out.println(sNames[i]);
        }
    }
}